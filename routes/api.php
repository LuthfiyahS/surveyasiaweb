<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\QuestionBank;
use App\Http\Controllers\Api\SurveyController;
use App\Http\Controllers\Api\SurveyjsController;
use App\Http\Controllers\Api\TemplateSurveyController;
use App\Http\Controllers\Api\AnalyticsController;
use App\Http\Controllers\Api\UploadFileController;
use App\Http\Controllers\Api\SurveyCategoryController;
use App\Http\Controllers\Api\QuestionBankSubTemplateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Api login untuk SPA
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function (Request $request) {
        return auth()->user();
    });

    //surveyjs endpoint
    
    Route::get('/surveyjs/{id}', [SurveyjsController::class, 'show']);
    Route::post('/surveyjs', [SurveyjsController::class, 'store']);

    
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});
Route::get('/surveyjs', [SurveyjsController::class, 'index']);
Route::put('/surveyjs/updatebycollectrespondent/{id}', [SurveyjsController::class, 'updateByCollectRespondent']);
//surveyjs sementara sampe login bisa kedetect
Route::post('/surveyjs-create', [SurveyjsController::class, 'store']);
Route::get('/surveyjs-showbyid/{id}', [SurveyjsController::class, 'showbyid']);
Route::get('/surveyjs-showbyuser/{id}', [SurveyjsController::class, 'showbyuser']);
Route::get('/surveyjs-shareablelink/{id}', [SurveyjsController::class, 'getShareableLinkById']);
Route::put('/surveyjs-updateslug/{id}', [SurveyjsController::class, 'updateSlug']);
Route::put('/surveyjs-updatetarget/{id}', [SurveyjsController::class, 'updateTarget']);
Route::delete('/surveyjs-deletebyid/{id}', [SurveyjsController::class, 'deletebyid']);
Route::post('/surveyjs-completed/', [SurveyjsController::class, 'completedSurvey']);
Route::delete('/surveyjs-deleteresultbyid/{id}', [SurveyjsController::class, 'deleteresultbyid']);


//template survey
Route::get('/template-survey', [TemplateSurveyController::class, 'show']);

//survey category endpoint ( termasuk survey category dari batch 2 )
Route::get('/surveycategory', [SurveyCategoryController::class, 'index']);
Route::get('/surveycategory/{id}', [SurveyCategoryController::class, 'show']);


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     Route::get('/profile', function(Request $request) {
//         return auth()->user();
//     });

// });

Route::apiResource('surveys', SurveyController::class);
Route::get('analytics/{survey}', [AnalyticsController::class, 'surveyAnalytics'])->name('api.analytics.show');
Route::apiResource('subtemplates', QuestionBankSubTemplateController::class);
Route::apiResource('survey/question-bank', QuestionBank::class)->only('index');
Route::post('uploader/image', [UploadFileController::class, 'imageUpload']); //->middleware('auth:sanctum'); // use sanctum for secure api



