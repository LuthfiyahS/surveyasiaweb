<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateSurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('template_surveys')->insert([
            'survey_id' => '9e3c5048-504a-4b53-970a-559864f94e7a',
            'survey_name' => 'Evaluasi Kursus',
            'survey_resultsId' => '17628820-f831-47db-81a5-62d85df45220',
            'survey_postId' => '64921e94-582c-4928-9ced-f2101d74e47d',
            'category_id' => 2
        ]);
    }
}

