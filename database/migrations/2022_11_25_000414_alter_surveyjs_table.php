<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterSurveyjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveyjs', function (Blueprint $table) {
            $table->string('description');
            DB::statement("ALTER TABLE surveyjs MODIFY status ENUM('active', 'closed', 'unpublished') DEFAULT 'unpublished'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveyjs', function (Blueprint $table) {
            $table->dropColumn('description');
            DB::statement("ALTER TABLE surveyjs MODIFY status ENUM('active', 'closed', 'unpublished') DEFAULT 'active'");
        });
    }
}
