<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollectToSurveyjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveyjs', function (Blueprint $table) {
             //for collect respondent
             $table->string('province', 50)->default('all');//region
             $table->string('city', 50)->default('all');//region
             $table->enum('gender', ['all', 'male', 'female'])->default('all'); //gender
             $table->integer('max_age')->default(100); //age
             $table->integer('min_age')->default(7);//age
             $table->integer('max_income')->default(100); //income //TODO cek kepake atau ngga 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveyjs', function (Blueprint $table) {
            //
        });
    }
}
