<?php

use App\Models\Surveyjs;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSurveyjsIdForeignToUsersSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_surveys', function (Blueprint $table) {
            $table->foreignIdFor(Surveyjs::class, 'surveyjs_id')->constrained('surveyjs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_surveys', function (Blueprint $table) {
            //
        });
    }
}
