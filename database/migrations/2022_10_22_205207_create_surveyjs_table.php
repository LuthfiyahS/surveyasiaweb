<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveyjs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();

            // field for surveyjs.io
            $table->string('surveyjs_id');
            $table->string('surveyjs_name');
            $table->string('surveyjs_resultsId');
            $table->string('surveyjs_postId');

            $table->integer('category_id');

            // estimate completion - time estimate
            $table->smallInteger('estimate_completion')->default('2');

            // target max respondent
            $table->integer('attempted')->default(0);

            // free survey
            $table->integer('max_attempt')->default(40);
 
            // survey status
            $table->enum('status', ['active', 'closed', 'unpublished'])->default('active');

            // reward point system
            $table->integer('reward_point')->default(0);

            // survey type to track wheter its free or premium
            $table->enum('type', ['free', 'paid'])->default('free');

            // shareable link
            $table->tinyInteger('shareable')->default(1);
            // shareable link
            $table->string('shareable_link')->nullable();
            // slug
            $table->string('slug')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveyjs');
    }
}
