<!doctype html>
<html lang="en">

<head>
  <title>Pemilu SurveyAsia</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  {{-- Favicon --}}
  <link rel='icon' type='image/jpg' href='/favicon_surveyasia-32.png' />
  <link rel='shortcut icon' type='image/x-icon' href='/favicon-surveyasia-32.ico' />

  <!-- Bootstrap CSS v5.2.0-beta1 -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
    integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

</head>

<body>
  <header>
    <center>
      <img class="img-fluid mb-2 mt-2" src="/assets/img/surveyasia_black.png" alt="" style="max-width: 200px"/>
    </center>
    <!-- place navbar here -->
  </header>

  <div id="app">
    <pemilu></pemilu>
 </div>

  <script src="{{ mix('/js/app.js') }}"></script>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
    integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
    integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous">
  </script>
</body>

</html>