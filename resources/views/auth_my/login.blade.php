@extends('layouts.footer')
@extends('layouts.base')
@extends('layouts.navbar')

@section('content')
    <div class="row" style="padding-top: -25px">
        <div class="col-md-7">
            <img src="/assets/img/bg-auth.png" alt="">
        </div>
        <div class="col-md-4 mt-5 p-3">
            <h5 class="mt-5">Log in</h5>
            <form action="{{ route('login') }}" method="POST">
                @csrf
                @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                        aria-label="Close"></button>
                </div>
                @endif

                @if (session('verified'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Your Account Has been Verified Successfully!
                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                        aria-label="Close"></button>
                </div>
                @endif

                @if (session('loginError'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('loginError') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                        aria-label="Close"></button>
                </div>
                @endif

                <div class="mt-4">
                    <label for="email" class="form-label fw-semibold">Email</label>
                    <input type="email" class="form-control @error('email')is-invalid @enderror" id="email"
                        name="email" aria-describedby="emailHelp" placeholder="Masukkan email Anda"
                        autofocus required value="{{ old('email') }}" />
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mt-3">
                    {{-- <label for="password" class="form-label fw-semibold">Password</label>
                    <input type="password" class="form-control" id="password" name="password"
                        placeholder="Masukkan password Anda" required /> --}}
                    <div class="form-group">
                        
                        <label for="password" class="form-label fw-semibold">Password</label>
                        <div class="input-group" id="show_hide_password" style="align-items: center">
                            <input type="password" class="form-control border-end-0" id="password" name="password" placeholder="Masukkan password Anda" required />
                            <div class="input-group-text border-start-0 bg-white" style="padding: 10px;">
                                <a href=""><i class="bi bi-eye-slash-fill text-muted" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between  mt-3">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input form-check-input-orange" type="checkbox"
                                name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label fs-14px" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <a href="{{ route('password.request') }}"
                        class="d-flex justify-content-end link-orange text-decoration-none fs-14px">Lupa
                        password?</a>
                </div>
                <button type="submit"
                    class="btn btn-orange radius-default w-100 text-white mt-3">Masuk</button>
            </form>
            <hr>           
            <p class="text-center text-muted fs-14px mt-4">atau masuk dengan akun lain</p>
                        <div class="d-flex justify-content-center gap-3">
                            <a href="{{ url('/auth/google') }}">
                                <div class="card card-btn radius-default py-2 px-3">
                                    <img src="{{ asset('assets/img/btn_google.png') }}" alt="Google" height="25" />
                                </div>
                            </a>
                            <a href="{{ url('/auth/facebook') }}">
                                <div class="card card-btn radius-default py-2 px-3">
                                    <img src="{{ asset('assets/img/btn_facebook.png') }}" alt="Facebook" height="25" />
                                </div>
                            </a>
                            <a href="{{ url('/auth/linkedin') }}">
                                <div class="card card-btn radius-default py-2 px-3">
                                    <img src="{{ asset('assets/img/btn_linkedin.png') }}" alt="LinkedIn" height="25" />
                                </div>
                            </a>
                        </div>
                        <div class="text-center mt-3">
                            <p class="text-muted fs-14px">Tidak memiliki akun? <a href="{{ route('choose-role') }}"
                                    class="link-orange text-decoration-none fw-semibold">Buat akun</a></p>
                        </div>
            
        </div>
    </div>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
@endsection