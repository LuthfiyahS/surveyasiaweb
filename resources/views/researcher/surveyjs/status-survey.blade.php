@extends('researcher.layouts.base-surveyjs')
@extends('researcher.layouts.navbar2')
@section('content')

    {{-- Breadcrumb --}}
    <section class="breadcrumb-contact mt-3 ms-5" id="breadcrumb-contact">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/researcher/surveys" class="link-yellow text-decoration-none">
                        <i class="fas fa-home fa-fw"></i>
                        Beranda
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href=" {{ route('researcher.surveyjs.creator', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Survey</a>
                </li>
                @if ($survey['type']=='paid')
                    <li class="breadcrumb-item">
                        <a href=" {{ route('researcher.surveyjs.collectRespondent', $survey['id']) }}"
                        class="link-secondary text-decoration-none">Kumpulkan Responden</a>
                    </li>
                @endif
                <li class="breadcrumb-item active">
                    <a href=" {{ route('researcher.surveyjs.statusSurvey', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Status Survey</a>
                </li>
                <li class="breadcrumb-item">
                    <a href=" {{ route('researcher.surveyjs.report', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Hasil Analisis</a>
                </li>
            </ol>
        </nav>
    </section>
    <hr class="mb-0">
    {{-- end Breadcrumb --}}

    <div id="app">
        <survey-status survey_id="{{ $survey['id'] }}"></survey-status>
    </div>

@endsection
