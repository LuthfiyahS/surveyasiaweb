@extends('researcher.layouts.base-surveyjs')
@extends('researcher.layouts.navbar2')
@section('content')

    {{-- Breadcrumb --}}
    <section class="breadcrumb-contact mt-3 ms-5" id="breadcrumb-contact">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/researcher/surveys" class="link-yellow text-decoration-none">
                        <i class="fas fa-home fa-fw"></i>
                        Beranda
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href=" {{ route('researcher.surveyjs.creator', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Survey</a>
                </li>
                @if ($survey['type']=='paid')
                    <li class="breadcrumb-item active">
                        <a href=" {{ route('researcher.surveyjs.collectRespondent', $survey['id']) }}"
                        class="link-secondary text-decoration-none">Kumpulkan Responden</a>
                    </li>
                @endif
                <li class="breadcrumb-item">
                    <a href=" {{ route('researcher.surveyjs.statusSurvey', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Status Survey</a>
                </li>
                <li class="breadcrumb-item">
                    <a href=" {{ route('researcher.surveyjs.report', $survey['id']) }}"
                       class="link-secondary text-decoration-none">Hasil Analisis</a>
                </li>
            </ol>
        </nav>
    </section>
    <hr class="mb-0">
    {{-- end Breadcrumb --}}
    {{-- <center>Ini halaman collect repondent</center> --}}
    <section>
        <div class="container py-5">
            <h3>Kumpulkan Responden</h3>
            <div class="border p-5 my-5">
                <h5 class="pt-2 pb-5">Langkah ke -1 : Siapa yang ingin kamu Survey?</h5>
                <div class="row">
                    <div class="col-md-6 col-lg-4 p-2"  onclick="myFunctionRegion()">
                        <div class="border card-collect text-center">
                            <h6 class="mb-4">Wilayah</h6>
                            <img src="{{ asset('assets/img/survey/region.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 p-2" onclick="myFunctionGender()">
                        <div class="border card-collect text-center">
                            <h6 class="mb-4">Jenis Kelamin</h6>
                            <img src="{{ asset('assets/img/survey/gender.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 p-2" onclick="myFunctionAge()">
                        <div class="border card-collect text-center">
                            <h6 class="mb-4">Umur</h6>
                            <img src="{{ asset('assets/img/survey/age.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
    
            <div id="region" class="border p-5 my-5" style="background-color: rgba(218, 218, 218, 0.3);">
                <h5 class="pt-2 pb-4">Wilayah</h5>
                <div class="row">
                    <div class="col-md-6">

                        <label for="prov" class="form-label">Provinsi</label>
                        <select class="form-control" name="prov" id="prov">
                            {{-- <option> - Pilih Provinsi -</option> --}}
                            @if ($survey['province'] == 'all')
                                <option value="{{$survey['province']}}" selected> Semua Provinsi</option>
                            @else
                                <option value="{{$survey['province']}}" selected> {{$survey['province']}}</option>
                                <option value="all" selected> Semua Provinsi</option>
                            @endif
                        </select>

                        <input type="hidden"
                            class="form-control @error('province') is-invalid @enderror"
                            id="province" name="province" aria-describedby="provinceHelp" required
                            value="{{ old('province') }}">

                        @error('province')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror

                    </div>
                    <div class="col-md-6">

                        <label for="cit" class="form-label">Kota</label>
                        <select class="form-control" name="cit" id="cit">
                            @if ($survey['city'] == 'all')
                                <option value="{{$survey['city']}}" selected> Semua Kota</option>
                            @else
                                <option value="{{$survey['city']}}" selected> {{$survey['city']}}</option>
                            @endif
                        </select>

                        <input type="hidden"
                            class="form-control @error('city') is-invalid @enderror" id="city"
                            name="city" aria-describedby="cityHelp" required value="{{ old('city') }}">

                        @error('city')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror

                    </div>
                </div>
                
            </div>

            <div id="gender" class="border p-5 my-5" style="background-color: rgba(218, 218, 218, 0.3);">
                <h5 class="pt-2 pb-4">Jenis Kelamin</h5>
                <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="gender2" value="all" @if ( $survey['survey_gender'] == 'all')
                        checked 
                    @endif>
                    <label class="form-check-label" for="gender2">
                      Semuanya 
                    </label>
                </div>
                <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="gender2" value="male" @if ( $survey['survey_gender'] == 'male')
                    checked 
                @endif>
                    <label class="form-check-label" for="gender2">
                      Laki - Laki
                    </label>
                </div>
                <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="gender2" value="female" @if ( $survey['survey_gender'] == 'female')
                    checked 
                @endif>
                    <label class="form-check-label" for="gender2">
                      Perempuan
                    </label>
                </div>
            </div>
    
            <div id="age" class="border p-5 my-5" style="background-color: rgba(218, 218, 218, 0.3);">
                <h5 class="pt-2 pb-5">Umur</h5>
                <div class="row">
                    <div class="col-md-6">
                        <p><b>Umur Minimal</b></p>
                        <div>
                            <form class="d-flex">
                                <input class="py-2 me-2 text-center"  type="number" name="minAgeInput" min="0" max="{{$survey['max_age'] }}" value="{{$survey['min_age'] }}" oninput="this.form.minAgeRange.value=this.value" />
                                <input id="myinput" style="width: 100%; margin-left: 10px" type="range" name="minAgeRange" min="0" max="{{$survey['max_age'] }}" value="{{$survey['min_age'] }}" oninput="this.form.minAgeInput.value=this.value" />
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p style="text-align:right"><b>Umur Maksimal</b></p>
                        <div style="text-align:right">
                            <form class="d-flex">
                                <input id="myinput" style="width: 100%; margin-right: 10px" type="range" name="maxAgeRange" min="{{$survey['min_age'] }}" max="100" value="{{$survey['max_age'] }}" oninput="this.form.maxAgeInput.value=this.value" />
                                <input class="py-2 ms-2 text-center"  type="number" name="maxAgeInput" min="{{$survey['min_age'] }}" max="100" value="{{$survey['max_age'] }}" oninput="this.form.maxAgeRange.value=this.value" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
    
            <div class="border p-5 my-5">
                <h5 class="pt-2 pb-5">Langkah ke -2 : Berapa Banyak responden yang dibutuhkan?</h5>
                <div class="text-center">
                    <form class="row">
                        <div class="col-md-2 mb-2">
                            <input class="py-2 text-center" type="number" name="amountInput" min="0" max="3000" value="{{$survey['max_attempt'] }}" oninput="this.form.amountRange.value=this.value" />
                        </div>
                        <div class="col-md-10">
                            <input id="myinput" type="range" name="amountRange" min="0" max="3000" value="{{$survey['max_attempt'] }}" oninput="this.form.amountInput.value=this.value" />
                        </div>
                    </form>
                </div>
            </div>
    
            <div class="border p-5 my-5">
                <div class="form-check mb-5">
                    <input class="form-check-input p-2" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label mb-2 fs-5 fw-semibold" for="flexCheckDefault">
                      Poin Tambahan
                    </label>
                    <br>
                    <input class="col-12 col-sm-6 col-md-4 text-center py-1" type="number" name="" id="" placeholder="Rp.100.000">
                </div>
    
                <div class="form-check">
                    <input class="form-check-input p-2" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label mb-2 fs-5 fw-semibold" for="flexCheckDefault">
                      Add Advance Analysis
                    </label>
                    <br>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                </div>
            </div>
        </div>
        </section>
        {{-- //TODO lanjut disini --}}
        <script type="text/javascript">
    
            // api kota
            var provSelect = document.querySelector('#prov');
            var provInput = document.querySelector('#province');
    
            var citSelect = document.querySelector('#cit');
            var citInput = document.querySelector('#city');
    
            getText("https://kodepos-2d475.firebaseio.com/list_propinsi.json?print=pretty");
    
            async function getText(url) {
                let provinces = await fetch(url);
                let pro = await provinces.json();
    
                Object.keys(pro).forEach(function(key) {
                    var op = document.createElement("option");
                    op.setAttribute("value", key);
                    var text = document.createTextNode(pro[key]);
                    op.appendChild(text);
                    provSelect.appendChild(op);
                });
    
            }
    
            provSelect.addEventListener('change', function() {
                provInput.value = provSelect.options[provSelect.selectedIndex].text;
    
                // citSelect.selectedIndex = 0;
                // disSelect.selectedIndex = 0;
    
                citSelect.replaceChildren();
                //disSelect.replaceChildren();
                //posInput.value = '';
                console.log('get prov ',provSelect.value);
                getText("https://kodepos-2d475.firebaseio.com/list_kotakab/" + provSelect.value + ".json?print=pretty");
    
                async function getText(url) {
                    let cities = await fetch(url);
                    let cit = await cities.json();
    
                    var ci = document.createElement("option");
                    ci.setAttribute("value", '');
                    var text = document.createTextNode('--Pilih Kota--');
                    ci.appendChild(text);
                    citSelect.appendChild(ci);

                    var c2 = document.createElement("option");
                    c2.setAttribute("value", 'all');
                    var text2 = document.createTextNode('Semua Kota');
                    c2.appendChild(text2);
                    citSelect.appendChild(c2);
    
                    Object.keys(cit).forEach(function(key) {
                        var ci = document.createElement("option");
                        ci.setAttribute("value", key);
                        var text = document.createTextNode(cit[key]);
                        ci.appendChild(text);
                        citSelect.appendChild(ci);
                    });
    
                }
    
            });
    </script>
@endsection


@section('js')
<script type="text/javascript">
    //untuk collect responden
function myFunctionRegion() {
    var x = document.getElementById("region");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
function myFunctionGender() {
    var x = document.getElementById("gender");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
function myFunctionAge() {
    var x = document.getElementById("age");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
//end untuk collect responden
</script>
@endsection