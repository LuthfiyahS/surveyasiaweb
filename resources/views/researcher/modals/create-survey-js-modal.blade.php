<!-- Modal Create Survey -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/') }}" >

<div class="modal fade" id="addSurveyJSModal" tabindex="-1" aria-labelledby="addSurveyModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-fullscreen-lg-down modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addSurveyModalLabel">Survey Baru</h5>
        @if($userSubscription == 1)
          <span class="badge bg-warning text-dark mx-2">Trial</span>
        @endif
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        @if($userSubscription == 1 && $surveyjs->count() >= 1)
          <div class="text-center justify-content-center">
            <p>Anda telah membuat 1 survey. Cek halaman
              <a href="{{ route('pricing') }}">pricing</a>
              untuk membuat lebih banyak survey!
            </p>
          </div>
        @else
          @if($userSubscription == 1)
            <div class="d-flex flex-row">
              <div class="mx-2 my-0">
                <span class="badge bg-info text-dark">Info</span>
              </div>
              <p>Anda berkesempatan membuat 1 survey. Cek halaman
                <a href="{{ route('pricing') }}">pricing</a>
                untuk membuat lebih banyak survey!
              </p>
            </div>
          @endif


          {{-- Step Wizard --}}
          <div class="stepwizard my-4">
            <div class="stepwizard-row setup-panel">
              <div class="stepwizard-step col-xs-3">
                <a href="#step-1" type="button" class="btn btn-success btn-circle-wizard" disabled="disabled">1</a>
                <p><small>Judul Survey</small></p>
              </div>
              <div class="stepwizard-step col-xs-3">
                <a href="#step-2" type="button" class="btn btn-secondary btn-circle-wizard disabled" disabled="disabled">2</a>
                <p><small>Tipe Survey</small></p>
              </div>
              <div class="stepwizard-step col-xs-3">
                <a href="#step-3" type="button" class="btn btn-secondary btn-circle-wizard disabled" disabled="disabled">3</a>
                <p><small>Additional Field</small></p>
              </div>
            </div>
          </div>

          <form action="{{ route('researcher.surveys.store') }}" method="POST" class="row my-1 needs-validation">

            @csrf
            <input type="hidden" name="survey" value="surveyjs">
            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

            <div class="mb-3 setup-content" id="step-1">
                <div class="form-floating mb-3">
                  <input type="text" class="form-control validasi-custom" name="title" id="title" required="required" placeholder="Judul">
                  <label for="title">Judul</label>
                </div>
                <div class="form-floating mb-3">
                  <textarea class="form-control validasi-custom" name="description" id="description" placeholder="Deskripsi"  required="required" style="height: 100px"></textarea>
                  <label for="description">Deskripsi</label>
                </div>

                <div class="d-grid d-md-flex justify-content-md-end">
                  <button class="btn btn-primary nextBtn" type="button">Next</button>
                </div>
            </div>

            <div class="mb-3 setup-content" id="step-2">
              <div class="mb-3">
                <label for="category_id" class="form-label">Kategori Survey</label>
                <select name="category_id" id="category_id" class="form-select validasi-custom" required="required">
                  <option value="" selected>Pilih Category</option>
                  @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="mb-3">
                <label for="tipe_survey" class="form-label">Tipe Survey</label>
                <select name="tipe_survey" class="form-select validasi-custom" id="tipe_survey" onchange="pilihTipeSurvey()" required="required">
                  <option value="" selected>Pilih Tipe</option>
                  <option value="free">Public</option>
                  <option value="paid">Member</option>
                </select>
              </div>

              <div class="d-grid d-md-flex justify-content-md-end">
                <button class="btn btn-primary nextBtn" type="button">Next</button>
              </div>
            </div>

            <div class="mb-3 setup-content" id="step-3">

              <div class="row mb-3 d-block" id="additional_info">
                <div class="d-flex flex-row">
                  <div class="mx-2 my-0">
                    <span class="badge bg-info text-dark">Info</span>
                  </div>
                  <p>Hanya bisa diisi untuk survey dengan tipe member</p>
                </div>
              </div>


              <div class="row mb-3">

                <div class="col-md-4">
                  <label for="estimate_completion" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Untuk membantu responden mengetahui berapa lama waktu yang diperlukan untuk menyelesaikan survey ini">
                    Estimasi Penyelesaian
                  </label>
                  <input type="number" class="form-control" max="20" min="1"
                         name="estimate_completion" id="estimate_completion"
                         placeholder="dalam satuan menit" disabled>

                  <div class="form-text">
                    <span class="text-orange">Tips!</span>
                    Disarankan tidak melebihi 10 menit
                  </div>
                </div>

                <div class="col-md-4">
                  <label for="max_attempt" class="form-label">Maksimum Responden</label>
                  <input name="max_attempt" id="max_attempt" type="number"
                         class="form-control" min="1" placeholder="40"
                         @if($user->subscription != null && $user->subscription->id == 1)
                           max="40"
                         @endif disabled>
                  @if ($user->subscription != null && $user->subscription->id == 1)
                    <div class="form-text">Maksimal 40 respondent untuk
                      <span class="fw-bold">FREE PACKAGE</span>
                    </div>
                  @else
                    <div class="form-text">Harap isi atau sistem akan menentukan 40 sebagai bawaan</div>
                  @endif
                </div>

                <div class="col-md-4">
                  <label for="reward_point" class="form-label">Jumlah Reward</label>
                  <input name="reward_point" id="reward_point" type="number" class="form-control" placeholder="0" disabled>
                </div>

              </div>

              <div class="row mt-5">
                <h6>Tentukan data respondent:</h6>
              </div>

              <div class="row mb-3">

                <div class="col-md-6">
                  <label for="province" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Tentukan lokasi provinsi respondent survey Anda">
                    Provinsi
                  </label>
                  <select name="province" class="form-select" id="province" disabled>
                    <option value="all" selected>All</option>
                  </select>
                </div>

                <div class="col-md-6">
                  <label for="city" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Tentukan lokasi kota respondent survey Anda">
                    Kota
                  </label>
                  <select name="city" class="form-select" id="city" disabled>
                    <option value="all" selected>All</option>
                  </select>
                </div>

              </div>

              <div class="row mb-3">

                <div class="col-md-4">
                  <label for="gender" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Tentukan gender respondent survey Anda">
                    Gender
                  </label>
                  <select name="gender" class="form-select" id="gender" disabled>
                    <option value="all" selected>All</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>

                <div class="col-md-4">
                  <label for="max_age" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Tentukan maksimal usia respondent survey Anda">
                    Max Age
                  </label>
                  <input name="max_age" id="max_age" type="number"
                         class="form-control" min="1" value="100" disabled>
                </div>

                <div class="col-md-4">
                  <label for="min_age" class="form-label" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="Tentukan minimal usia respondent survey Anda">
                    Min Age
                  </label>
                  <input name="min_age" id="min_age" type="number"
                         class="form-control" min="1" value="17" disabled>
                </div>

              </div>

              <div class="d-grid d-md-flex justify-content-md-end">
                <button class="btn btn-success" type="submit">Finish!</button>
              </div>
            </div>

          </form>
        @endif
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  const provinsi = document.getElementById("province");
  const kota = document.getElementById("city");

  fetch(`https://kanglerian.github.io/api-wilayah-indonesia/api/provinces.json`)
          .then(response => response.json())
          .then(provinces => {
            const data = provinces
            let temp = '<option value="all" selected>All</option>'
            data.forEach(element => {
              temp += `<option data-reg="${element.id}" value="${element.name}">${element.name}</option>`
            })

            provinsi.innerHTML = temp
          });

  provinsi.addEventListener('change', (e) => {
    const idProvinsi = e.target.options[e.target.selectedIndex].dataset.reg
    fetch(`https://kanglerian.github.io/api-wilayah-indonesia/api/regencies/${idProvinsi}.json`)
            .then(response => response.json())
            .then(cities => {
              const data = cities
              let temp = '<option value="all" selected>All</option>'
              data.forEach(element => {
                temp += `<option data-dist="${element.id}" value="${element.name}">${element.name}</option>`
              })

              kota.innerHTML = temp
            });
  })

  function pilihTipeSurvey() {

    const tipeSurvey = document.getElementById("tipe_survey").value;

    /* Field Only Survey Member */
    const estimateCompletion = document.getElementById("estimate_completion");
    const maxAttempt = document.getElementById("max_attempt");
    const rewardPoint = document.getElementById("reward_point");

    const province = document.getElementById("province");
    const city = document.getElementById("city");
    const gender = document.getElementById("gender");
    const maxAge = document.getElementById("max_age");
    const minAge = document.getElementById("min_age");

    const additionalInfo = document.getElementById("additional_info")

    if (tipeSurvey === "paid") {
      additionalInfo.classList.remove("d-block")
      additionalInfo.classList.add("d-none")

      estimateCompletion.disabled = false;
      maxAttempt.disabled = false;
      rewardPoint.disabled = false;

      province.disabled = false;
      city.disabled = false;
      gender.disabled = false;
      maxAge.disabled = false;
      minAge.disabled = false;

      estimateCompletion.required = true;
    } else {
      additionalInfo.classList.remove("d-none")
      additionalInfo.classList.add("d-block")

      estimateCompletion.disabled = true;
      maxAttempt.disabled = true;
      rewardPoint.disabled = true;

      province.disabled = true;
      city.disabled = true;
      gender.disabled = true;
      maxAge.disabled = true;
      minAge.disabled = true;

      estimateCompletion.required = false;
    }
  }

  $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-active-wizard');
        navListItems.removeClass('btn-success');
        navListItems.addClass('btn-secondary');
        $item.addClass('btn-active-wizard');
        $item.removeClass('btn-secondary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
      }
    });

    allNextBtn.click(function () {
      var curStep = $(this).closest(".setup-content"),
              curStepBtn = curStep.attr("id"),
              nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
              curInputs = curStep.find("input[type='text'],textarea,select"),
              isValid = true;

      $(".validasi-custom").removeClass("is-invalid");
      for (var i = 0; i < curInputs.length; i++) {
        if (!curInputs[i].validity.valid) {
          isValid = false;
          $(curInputs[i]).closest(".validasi-custom").addClass("is-invalid");
        }
      }

      if (isValid) nextStepWizard.removeClass("disabled").removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
  });

</script>