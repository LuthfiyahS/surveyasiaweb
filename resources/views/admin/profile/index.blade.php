{{-- @extends('layouts.footer') --}}
@extends('layouts.base')
@include('admin.component.header')

@section('content')

<h3 class="pt-5 ms-5 fw-bold">Akun Saya</h3>

{{-- User Dashboard --}}
<section class="user-profile pt-4 pb-5 mx-5" id="user-profile">
    <div class="row">
        <div class="col-lg-12 shadow pt-4 pb-5 px-5" style="border-radius: 16px;">
    @if (session('success'))
    <div class="alert alert-success alert-dismissible show fade">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible show fade">
        {{ session('error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="row">
        <div class="col-md-2">
            @if (Auth::user()->avatar == null)
            <img src="{{ asset('assets/img/noimage.png') }}" alt="Profile Picture" width="110"
                class="img-fluid d-block mb-2 ms-3 rounded-pill object-fit-cover">
            @elseif (Auth::user()->provider_name != null)
            <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->nama_lengkap }}" width="110"
                class="img-fluid d-block mb-2 ms-3 rounded-pill object-fit-cover">
            @else
            <img src="{{ asset('storage/' . \Auth::user()->avatar) }}" width="110" height="110"
                alt="{{ Auth::user()->nama_lengkap }}"
                class="img-fluid d-block mb-2 ms-3 rounded-pill object-fit-cover" name="avatar">
            @endif
        </div>
        <div class="col">
            <h3 class="fw-semibold">{{ $user->nama_lengkap }}</h3>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-3">
        <a href="edit-profile" class="btn btn-outline-orange radius-default">Edit Profil</a>
    </div>

    <div class="row mt-4">
        <div class="col-md-2">
            <p>Nama</p>
        </div>
        <div class="col">
            <p class="fw-semibold">{{ $user->nama_lengkap }}</p>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="col-md-2">
            <p>Email</p>
        </div>
        <div class="col">
            <p class="fw-semibold">{{ Auth::user()->email }}</p>
            <div class="d-flex">
                @if (Auth::user()->email_verified_at != null)
                <p class="text-success me-3">Verified</p>
                @else
                <p class="text-orange me-3">Not Verified</p>
                <a href="#" class="link-orange text-decoration-none">Resend
                    Email</a>
                @endif
            </div>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-md-2">
            <p>No. Telepon</p>
        </div>
        <div class="col">
            <p class="fw-semibold">{{ $user->profile->telp ?? $user->telp }}</p>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-md-2">
            <p>Pekerjaan</p>
        </div>
        <div class="col">
            <p class="fw-semibold">{{ $user->profile->job ?? $user->job }}</p>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-md-2">
            <p>Alamat</p>
        </div>
        <div class="col">
            <p class="fw-semibold">
                {{ $user->profile->address ?? $user->address }}
            </p>
        </div>
        <hr>
    </div>
    </div>
</div>
</section>
{{-- End User Profile --}}

@endsection