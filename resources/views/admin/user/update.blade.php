@extends('admin.layouts.base')

@section('css')
<link rel="stylesheet" href="{{ asset('css/admin-dashboard.css') }}">
<style>
    body {
        background-color: #F7FAFC;
    }
</style>
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-2 nopadding">
            @include('admin.component.sidebar')
        </div>
        <div class="col-10 nopadding">
            @include('admin.component.header')

            <div class="container mt-4">
                <div class="row">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="container py-4">
                        <div class="container ">
                            <div class="row">
                                <div class="col">
                                    <a href="{{ route('admin.users.index') }}"
                                        class="mb-2 text-dark h6 text-decoration-none">
                                        <i class="bi bi-chevron-left pe-2"></i>
                                        Kembali
                                    </a>
                                    <h4 class=" text-center py-3" style="fw-bold">Update Pengguna</h4>
                                </div>
                            </div>
                        </div>

                        <div class="card border-0 bg-white p-3" style="border-radius: 20px;">
                            <nav class="user-tabs">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active text-secondary" id="kontak-tab"
                                        data-bs-toggle="tab" data-bs-target="#kontak" type="button" role="tab"
                                        aria-controls="kontak" aria-selected="true">Data Pengguna</button>
                                </div>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="kontak" role="tabpanel"
                                        aria-labelledby="kontak-tab">
                                        <form action="{{ route('admin.users.update', $user->id) }}" method="post">
                                            @csrf
                                        <div class="col-12 text-center">
                                            <br>
                                            <p class=" text-center " style="fw-bold; font-size: 13px;">Foto
                                                Profil</p>
                                                @if ($user->avatar !=null)
                                                <img src="{{ asset('storage/' . $user->avatar) }}" alt="Profile Picture" width="70" height="70"
                                                class=" mb-2 rounded-pill profile-picture-preview object-fit-cover">
                                                @else
                                                <img src="{{ asset('assets/img/photo-neil-seims.jpg') }}"
                                                    alt="Profile Picture" width="70" height="70"
                                                    class=" mb-2 rounded-pill profile-picture-preview object-fit-cover">
                                                @endif
                                        </div>
                                        <br>
                                        <div class="col-12 text-center">
                                            <ins class=" text-center "
                                                style="fw-bold; font-size: 16px;">
                                            
                                                @if($user->role_id == 1)
                                                    Administrator
                                                @elseif ($user->role_id == 2)
                                                    Researcher
                                                @elseif ($user->role_id == 3)
                                                    Responden
                                                @endif
                                            </ins>
                                        </div>
                                        </br>
                                        <div class="mb-3">
                                            <label for="exampleFormControlInput1"
                                                class="form-label">Nama</label>
                                            <input type="text" class="form-control"
                                                id="exampleFormControlInput1" name="nama_lengkap"
                                                value="{{ $user->nama_lengkap }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleFormControlInput1"
                                                class="form-label">Telepon</label>
                                            <input type="text" class="form-control"
                                                id="exampleFormControlInput1" name="telp"
                                                value="{{ $user->telp }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleFormControlInput1"
                                                class="form-label">Email</label>
                                            <input type="text" class="form-control"
                                                id="exampleFormControlInput1" name="email"
                                                value="{{ $user->email }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleFormControlInput1"
                                                class="form-label">Role User</label>
                                            <select name="role_id" id="" class="form-control" required>
                                                {{-- <option value="{{ $user->role_id }}" selected>-- Pilih Role User --</option> --}}
                                                <option value="{{ $user->role_id }}" selected>-- Pilih Role User --</option>
                                                <option value="1">Administrator</option>
                                                <option value="2">Researcher</option>
                                                <option value="3">Responden</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-orange text-white  px-5">Update</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                        </div>















                            @endsection