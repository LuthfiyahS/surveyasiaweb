<!doctype html>
<html lang="en">

<head>
  <title>Title</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <style lang="scss">
        .btn-pomegranate {
            background-color: #EF4C29;
            color: #fff;
            display: inline-block;
            text-align: center;
            border: 1px solid transparent;
            padding: 0.50rem 0.50rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.35rem;
        }
        .btn-pomegranate:hover {
            background-color: #FF7C60;
            color: #fff;
        }
        .text-pomegranate {
            color: #EF4C29;
        }
        </style>

</head>

<body>
    <div class="row">
        <div class="col-7">
            <img src="/assets/img/bg-auth.png" alt="">
        </div>
        <div class="col-4 mt-5">
            <h5 class="mt-5">Log in</h5>
            <div class="form-group">
                <label for="exampleFormControlInput1">Email</label>
                <input type="email" class="form-control" placeholder="masukan email anda">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Password</label>
                <input type="password" class="form-control" placeholder="masukan password anda">
            </div>
            <div class="d-flex justify-content-end text-pomegranate">lupa password?</div>
            <button class="btn-pomegranate w-100 rounded mt-3">Log in</button>
            <hr>           
            <p class="text-center">OR</p>
            <div class="d-flex justify-content-center">
                <a href="" class="btn"><img class="w-50" src="assets/img/btn_google.png" alt=""></img></a>
                <a href="" class="btn"><img class="w-50" src="assets/img/btn_linkedin.png" alt=""></img></a>
                <a href="" class="btn"><img class="w-50" src="assets/img/btn_facebook.png" alt=""></img></a>
            </div>
            <p class="text-center mr-2">Don’t you have an Account?
                <a href="" class="ml-2 text-pomegranate">Sign up</a>
            </p>
            
        </div>
    </div>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>