/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// import { vue } from 'laravel-mix';
import VueRouter from 'vue-router';
import Routes from './router/index';


window.Vue = require('vue').default;

Vue.use(VueRouter);

const router = new VueRouter({
    routes: Routes
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('survey-creator', require('./pages/SurveyCreator.vue').default);
Vue.component('ticket', require('./Main.vue').default);
Vue.component('user-management', require('./UserManagementMain').default);
Vue.component('vik', require('./Vik.vue').default);
Vue.component('vik-admin', require('./VIKAdmin.vue').default);
Vue.component('sidebar', require('./components/Sidebar.vue').default); 
Vue.component('dashboard-vik', require('./Vik.vue').default);
Vue.component('survey-new', require('./SurveyMain').default);
Vue.component('pemilu', require('./pages/survey/Pemilu.vue').default);
Vue.component('survey-run', require('./pages/survey/View/SurveyRunner').default);

// v2
Vue.component('survey-creator-new', require('./pages/survey/Creator/v2/SurveyCreatorNew').default);
Vue.component('survey-report', require('./pages/survey/Creator/v2/ResultSurveyNew').default);
Vue.component('survey-share', require('./pages/survey/Creator/v2/ShareSurveyNew').default);
Vue.component('survey-status', require('./pages/survey/Creator/v2/StatusSurveyNew').default);

// admin
Vue.component('survey-preview', require('./pages/survey/Admin/Preview').default);
Vue.component('survey-result-admin', require('./pages/survey/Admin/ResultSurveyAdmin').default);

//auth
Vue.component('auth', require('./pages/Login').default)

import JwPagination from 'jw-vue-pagination';
import Vue from 'vue';
Vue.component('jw-pagination', JwPagination);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



const app = new Vue({
    el: '#app',
    router: router
});

