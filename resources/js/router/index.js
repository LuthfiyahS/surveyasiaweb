//CS
import Assigment from "../pages/Assigment.vue";
import AllTickets from "../pages/AllTicket.vue";
import Reports from "../pages/Reports.vue";
import Conversation from "../pages/Conversation.vue";
//User Management
import DataUser from "../pages/DataUser";
import DataVerifikasi from "../pages/DataVerifikasi";
//VIK
import LandingPage from "../pages/LandingPage";
import Event from "../pages/Event";
import EventDetail from "../pages/EventDetail";
import DashboardVik from "../pages/Dashboard.vue";
import DataVIK from "../pages/DataVIK";
import TopicVIK from "../pages/TopicVIK";
import ArtikelData from "../pages/ArtikelData";
import Laporan from "../pages/Laporan";
import VikAsiaTv from "../pages/VikAsiaTv";

//VIK Admin
import EventAdmin from "../pages/EventAdmin.vue";
import DraftEventAdmin from "../pages/DraftEventAdmin.vue";
import CreateArticle from "../pages/CreateArticle.vue";
import EventDetailAdmin from "../pages/EventDetailAdmin";
import AddEvent from "../pages/AddEvent";

//VIK Redaksi
import RequestDataVIK from "../pages/RequestDataVIK.vue";

//Laporan VIK
import DetailLaporan from "../pages/DetailLaporan.vue";

//news
import NewsVIK from "../pages/vik/news/NewsVIK.vue";

// Survey New
import SurveyCreatorByID from "../pages/survey/Creator/SurveyCreator.vue";
import ListSurveyCreator from "../pages/survey/Creator/ListSurveyCreator";
import ListSurvey from "../pages/survey/Creator/ListSurvey.vue";
import CreateSurvey from "../pages/survey/Creator/CreateSurvey.vue";
import ResultByIDSurvey from "../pages/survey/Creator/ResultSurvey.vue";
import ShareSurveyByIDSurvey from "../pages/survey/Creator/ShareSurvey.vue";

export default [

  //admin cs
  { path: "/assigment", component: Assigment },
  { path: "/allticket", component: AllTickets },
  { path: "/report", component: Reports },
  { path: "/conversation", component: Conversation },

  // admin user management
  { path: "/data-user", component: DataUser },
  { path: "/data-verifikasi", component: DataVerifikasi },

  //vik user
  // { path: "/", component: LandingPage },
  { path: "/landing-page", component: LandingPage },

  //vik event
  { path: "/event", component: Event },
  { path: "/event/detail", component: EventDetail },
  { path: "/data", component: DataVIK }, //unused
  { path: "/topic", component: TopicVIK },
  // { path: "/topic/:id/:name", component: TopicVIK }, //console.log(this.$route.params.id) buat cek idnya
  { path: "/dashboard-vik", component: DashboardVik }, //unused
  { path: "/artikel-data", component: ArtikelData },
  { path: "/reports", component: Laporan },
  { path: "/vikasiatv", component: VikAsiaTv },

  //vik-admin
  { path: "/event-admin", component: EventAdmin },
  { path: "/event-admin/draft", component: DraftEventAdmin },
  { path: "/event-admin/detail-event", component: EventDetailAdmin },
  { path: "/event-admin/add-event", component: AddEvent },
  { path: "/create-article", component: CreateArticle },

  //vik-redaksi
  { path: "/request-data", component: RequestDataVIK },

  //vik-laporan
  { path: "/reports", component: Laporan },
  { path: "/reports/detail", component: DetailLaporan },

  //vik news
  { path: "/news", component: NewsVIK },

  // survey-new (public+advance)
  {
    path: '/',
    name: 'listsurvey',
    component: ListSurvey,
  },
  {
    path: '/creator',
    name: 'creator',
    component: ListSurveyCreator,
  },
  {
    path: '/create-survey',
    name: 'create-survey',
    component: CreateSurvey,
  },
  {
    path: '/creator/:id',
    name: 'CreatorByID',
    component: SurveyCreatorByID
  },
  {
    path: '/result-survey/:id',
    name: 'ResultByIDSurvey',
    component: ResultByIDSurvey
  },
  {
    path: '/share-survey/:id',
    name: 'ShareSurveyByIDSurvey',
    component: ShareSurveyByIDSurvey
  },

];

