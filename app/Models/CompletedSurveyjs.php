<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompletedSurveyjs extends Model
{
    use HasFactory;

    protected $fillable = [
        'surveyjs_id',
        'respondent_id'
    ];
}
