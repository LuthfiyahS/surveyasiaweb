<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Surveyjs extends Model
{
    use HasFactory, SoftDeletes, HasRelationships;

    protected $fillable = [
        'user_id',

        // Ambil dari surveyjs
        'surveyjs_id','surveyjs_name','surveyjs_resultsId','surveyjs_postId',

        'description', 'category_id', 'estimate_completion', 'attempted', 'max_attempt', 'status', 'reward_point',
        'type', 'shareable', 'shareable_link', 'slug','province','city','district','gender','max_age','min_age','max_income'
    ];

    public function user()
    {
        # code...
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        # code...
        return $this->belongsTo(SurveyCategory::class, 'category_id');
    }
}
