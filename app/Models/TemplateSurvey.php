<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateSurvey extends Model
{
    use HasFactory;

    protected $fillable = [
        'survey_id','survey_name','survey_resultsId','survey_postId', //ambil dari surveyjs
        'category_id',
        'created_at', 'updated_at'
    ];

    public function category()
    {
        # code...
        return $this->belongsTo(SurveyCategory::class, 'category_id');
    }
}
