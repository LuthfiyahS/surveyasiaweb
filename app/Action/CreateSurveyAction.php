<?php

namespace App\Action;

use App\Http\Requests\CreateSurveyRequest;
use App\Models\Survey;
use App\Models\Surveyjs;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CreateSurveyAction
{
    public function execute(CreateSurveyRequest $request)
    {
        # code...
        $estimateCompletion = $request->estimate_completion;
        $maxAttempt = $request->max_attempt == null ? 40 : $request->max_attempt;
        $shareable = $request->has('shareable') ? 1 : 0;
        $reward = $request->reward_point == null ? 0 : $request->reward_point;
        $category = $request->category_id;

        $survey = Survey::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $request->user_id,
            'category_id' => $category,
            'shareable' => $shareable,
            'signature' => Str::random(4).$request->title,
            'slug' => Str::slug($request->title, '-'),
            'max_attempt' => $maxAttempt,
            'reason_deny' => null,
            'estimate_completion' => $estimateCompletion,
            'reward_point' => $reward
        ]);

        if (!$survey) {
            return redirect()->back()->withErrors('Internal Server Error');
        }

        return redirect()->back()->with('success', 'Survey Created!');
    }

    public function executeSurveyJS(CreateSurveyRequest $request)
    {
        // Setup Guzzle HTTP Client
        $client = new Client();

        // Prepare Request
        $requestCreate = new Request('POST', 'https://api.surveyjs.io/private/Surveys/create');

        // Send Request
        try {
            $response = $client->send($requestCreate, [
                'query' => [
                    'accessKey' => env('ACCESS_KEY_SURVEYJS'),
                    'name' => $request->title
                ]
            ]);
        } catch (GuzzleException $e) {
            return redirect()->back()->withErrors('Internal Server Error');
        }

        // Read Response
        $response_body = $response->getBody();
        // Convert JSON to Object
        $response_body = json_decode($response_body);

        // Prepare save to database
        $request['surveyjs_name'] = $request->title;
        $request['surveyjs_id'] = $response_body->Id;
        $request['surveyjs_resultsId'] = $response_body->ResultId;
        $request['surveyjs_postId'] = $response_body->PostId;

        $request['description'] = $request->description;

        $request['type'] = $request->tipe_survey;

        $request['estimate_completion'] = $request->estimate_completion == null ? 1 : $request->estimate_completion;
        $request['max_attempt'] = $request->max_attempt == null ? 40 : $request->max_attempt;
        $request['reward_point'] = $request->reward_point == null ? 0 : $request->reward_point;
        $request['shareable'] = $request->has('shareable') ? 1 : 0;

        $request['province'] = $request->province == null ? 'all' : $request->province;
        $request['city'] = $request->city == null ? 'all' : $request->city;
        $request['gender'] = $request->gender == null ? 'all' : $request->gender;
        $request['max_age'] = $request->max_age == null ? 100 : $request->max_age;
        $request['min_age'] = $request->min_age == null ? 7 : $request->min_age;

        $request['slug'] = Str::slug($request->title, '-') . '-' . substr($response_body->Id, 0 ,5);
        $request['shareable_link'] = env('APP_URL') . $request['slug'];

        // Save database with eloquent
        $surveyJS = Surveyjs::create($request->all());

        // Failed
        if (!$surveyJS) {
            return redirect()->back()->withErrors('Internal Server Error');
        }

        // Success
        return redirect()->back()->with('success', 'Survey Created!');
    }
}