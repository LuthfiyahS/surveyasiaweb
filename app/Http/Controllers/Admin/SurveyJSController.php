<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategorySubcriptions;
use App\Models\SurveyCategory;
use App\Models\Surveyjs;
use App\Models\UsersSubscriptions;
use Illuminate\Http\Request;

/**
 * Controller handle service SurveyJS Admin
 *
 * Since Batch 3
 */
class SurveyJSController extends Controller
{
    public $title = "Survey";

    public function showListSurveyJS() {
        $surveyJSPending = Surveyjs::where('status', 'unpublished')->with('user')->orderBy('updated_at', 'asc')->get();
        $surveysDeny = Surveyjs::where('status', 'closed')->with('user')->latest()->get();
        $surveysAcc = Surveyjs::where('status', 'active')->with('user')->latest()->get();

        $data = [
            'title' => $this->title,
            'surveysPending' => $surveyJSPending,
            'surveysAcc' => $surveysAcc,
            'surveysDeny' => $surveysDeny,
        ];
        return view('admin.surveyjs.index', $data);
    }

    public function showPreviewSurveyJS($id) {
        $survey = Surveyjs::select('surveyjs_name', 'surveyjs_id', 'surveyjs_postId')->where('id', $id)->get()->first();

        $name = $survey['surveyjs_name'];
        $surveyId = $survey['surveyjs_id'];
        $postId = $survey['surveyjs_postId'];

        return view('admin.surveyjs.preview')->with('survey', ['id' => $surveyId, 'name' => $name, 'post_id' => $postId]);
    }

    public function showSurveyAccepted($id) {
        $surveySelect = Surveyjs::where('id', $id)->with('user')->first();
        $categoryName = SurveyCategory::where('id', $surveySelect->category_id)->first();
        $subscriptionSelect = UsersSubscriptions::where('user_id', $surveySelect->user->id)->first();
        $categorySubsName = $subscriptionSelect ? CategorySubcriptions::where('id', $subscriptionSelect->category_id)->first()->title : 'Tidak Berlangganan';

        return view('admin.surveyjs.acc', [
            'title' => $this->title,
            'survey' => $surveySelect,
            'category_survey' => $categoryName->name,
            'category_subs' => $categorySubsName,
        ]);
    }

    public function acceptSurvey($id) {
        Surveyjs::where('id', $id)->update(['status' => 'active']);

        return redirect('/admin/surveyjs');
    }

    public function denySurvey(Request $request, $id) {
        Surveyjs::where('id', $id)->update(
            ['status' => 'closed', 'reason_deny' => $request->reason]
        );

        return redirect('/admin/surveyjs');
    }

    public function cancelDeny($id) {
        Surveyjs::where('id', $id)->update(
            ['status' => 'unpublished', 'reason_deny' => null]
        );

        return redirect('/admin/surveyjs');
    }
}
