<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Notifications\Custom;
use App\Jobs\SendCustomEmailJob;
use App\Mail\WelcomeNewUserMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CategorySubcriptions;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendWelcomeMailToUserJob;
use App\Http\Requests\PersonalDataRequest;
use App\Models\Answer;
use App\Models\Survey;
use App\Models\UsersProfile;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate as Gate;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Gate as FacadesGate;
use Illuminate\Contracts\Auth\Access\Gate as AccessGate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $title = "Pengguna";
    public function index()
    {
        //abort_if(!$user->can('viewAny', User::class), 403, 'Unauthorized');
        $users = User::with('subscription', 'profile')->latest()->get();
        $subscriptions = Subscription::with('users')->get(); //user subscriptions
        $rolesWithPermissions = Role::with('permissions')->get();
        $rolesWithUsers = Role::with('users')->get();
        $usersWithRole = User::with('role')->get();
        //$usersWithPermissions = User::with('permissions')->get();
        $month = Carbon::now()->format('m');
        // dd(now());

        // Jumlah user di bulan ini
        $users_month = count(User::whereRaw('MONTH(created_at) = ' . $month)->get());
        
        //Jumlah total user
        $totalUser = User::count();

        // dd($jumlahUserBulanIni);

        // test notifiy all users
        // Notification::send(User::all(), new Custom);
        $data = [
            'title' => $this->title,
            'users' => $users,
            'users_total' => $totalUser,
            'users_month' => $users_month,
            'usersubs' => $subscriptions,
        ];

        // dd($data);

        return view('admin.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (!Gate::allows('create', $request->user())) {
            abort(403, 'Unauthorized');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Answer $respondent_id)
    {
        // $data = User::find($user);
        // return view('admin.user.update', $data);


        # code...
        // return User::with('surveys.answers.question')->where('id', $user->id)->get();
        // $gg = DB::connection('mysql')->select("SELECT

        // ");
        return view('admin.user.update', [
            'title' => 'Profile',
            'user' => $user,
            // 'users' => $user->with('surveys')->whereId($user->id)->get()
            'users' => Survey::with('user')->where('user_id', $user->id)->get(),
            // 'respondents' => User::with('surveys.answers.question')->where('id', $user->id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
        $profile = $request->validate([
            'nama_lengkap' => ['required'],
            'telp' => ['required', 'numeric'],
            'email' => ['required'],
            'role_id' => ['required']
        ]);
        $nama = User::find($id);
        User::where('id', $id)->update($profile);
        return redirect('admin/users/')->with('toast_success', 'Pengguna berhasil diubah');
        } catch (QueryException $e) {
            return redirect('admin/users')->with('toast_error',  'Data tidak berhasil diubah!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        //$this->authorize('delete', $user);
        // if (!Gate::allows('delete', $user)) {
        //     abort(403, 'Unauthorized');
        // }
        $user->surveys()->delete();
        $user->transactions()->delete();
        $user->surveyHistories()->delete();

        $user->delete();

        return back()->with('status', 'Pengguna berhasil dihapus');
    }

    public function notify(User $user)
    {
        # code...
        // SendCustomEmailJob::dispatch($user);
        // $user->notify(new Custom);
        SendWelcomeMailToUserJob::dispatch($user)->onQueue('emails');
        // Mail::to($user)->send(new WelcomeNewUserMail);
        return redirect('/admin/users')->with('success', 'Notification sent');
    }

    public function profile(User $user, Answer $respondent_id)
    {
        # code...
        // return User::with('surveys.answers.question')->where('id', $user->id)->get();
        // $gg = DB::connection('mysql')->select("SELECT

        // ");
        return view('admin.user.profile', [
            'title' => 'Profile',
            'user' => $user,
            // 'users' => $user->with('surveys')->whereId($user->id)->get()
            'users' => Survey::with('user')->where('user_id', $user->id)->get(),
            // 'respondents' => User::with('surveys.answers.question')->where('id', $user->id)->get()
        ]);
    }

    public function dataVerify()
    {
        //
        //abort_if(!$user->can('viewAny', User::class), 403, 'Unauthorized');
        $users = User::with('subscription')->latest()->paginate(10);
        $subscriptions = Subscription::with('users')->get(); //user subscriptions
        $rolesWithPermissions = Role::with('permissions')->get();
        $rolesWithUsers = Role::with('users')->get();
        $usersWithRole = User::with('role')->get();
        //$usersWithPermissions = User::with('permissions')->get();
        $month = Carbon::now()->format('m');
        // dd(now());
        // Jumlah user di bulan ini
        $users_month = count(User::whereRaw('MONTH(created_at) = ' . $month)->get());
        //Jumlah total user
        $totalUser = User::count();

        // dd($jumlahUserBulanIni);

        //test notifiy all users
        // Notification::send(User::all(), new Custom);
        $data = [
            'title' => 'Data Verification',
            'users' => $users,
            'users_total' => $totalUser,
            'users_month' => $users_month,
            'usersubs' => $subscriptions,
        ];

        // dd($data);
        return view('admin.dataVerify.index', $data);
    }
    public function status($id, $status)
    {
        // $user = $request->validate([
        //     'status' => 'required'
        // ]);
        User::where('id', $id)->update(['status' => $status]);
        // jika user diacc
        if ($status == 2) {
            return back()->with('status', 'Pengguna disetujui');
        } else {
            return back()->with('status', 'Pengguna ditolak');
        }
    }

    public function accUser($id)
    {
        User::where('id', $id)->update(['status' => 2]);
        return back()->with('status', 'Pengguna disetujui');
    }
    // public function rejectUser($id)    
    // {
    //     User::where('id', $id)->update([
    //         'status' => 2,
    //         'desc_reject' =>
    //     ]);
    // }

    public function adminprofile()
    {
        $user = User::with(['profile'])->where('id', Auth::user()->id)->first();
        $title = 'Profil Akun';
        return view('admin.profile.index', compact('user','title' ));
    }

    public function editadminprofile()
    {
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $title = 'Edit Profil Akun';
        return view('admin.profile.editprofile', compact('user', 'title'));
    }


    public function updateadminprofile(Request $request, $id)
    {
        validator::make($request->all(), [
            'nama_lengkap' => 'max:255|required',
            'telp' => 'nullable|digits_between:10,13',
            'job' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'email' => 'max:255|email:dns|required',
            'avatar' => 'nullable|image|mimes:jpg,jpeg,png|max:5000'
        ])->validate();
        $user = User::with('profile')->findOrFail($id);

        $user->nama_lengkap = $request->get('nama_lengkap');
        $user->email = $request->get('email');
        $user->avatar = $request->get('avatar');
        $user->telp = $request->get('telp');
        $user->job = $request->get('job');
        $user->address = $request->get('address');

        if ($user->profile) {
            $user->profile->nama_lengkap = $request->get('nama_lengkap');
            $user->profile->telp = $request->get('telp');
            $user->profile->job = $request->get('job');
            $user->profile->address = $request->get('address');

            $user->profile->save($request->all());
        }

        if ($request->file('avatar')) {
            if ($user->avatar && file_exists(storage_path('app/public') . $user->avatar)) {
                Storage::delete('public/' . $user->avatar);
            }
            $file = $request->file('avatar')->store('avatars', 'public');
            $user->avatar = $file;
        }

        $user->save($request->all());


        return redirect()->route('admin.profile')->with('success', 'Ubah profil berhasil.');
    }
}
