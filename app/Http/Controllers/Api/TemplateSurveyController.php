<?php

namespace App\Http\Controllers\Api;

use App\Models\TemplateSurvey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class TemplateSurveyController extends Controller
{
    public function show() {
        $survey = TemplateSurvey::all();
        if($survey) {
            $response = [
                'message' => 'List Template Survey',
                'data' => $survey,
            ];
        }else{
            $response = [
                'message' => 'List Template Survey not found',
            ];
        }
        
        return response()->json($response, Response::HTTP_OK);
    }
}
