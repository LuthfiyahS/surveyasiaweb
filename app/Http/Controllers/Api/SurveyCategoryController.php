<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SurveyCategory;
use Symfony\Component\HttpFoundation\Response;

class SurveyCategoryController extends Controller
{
    public function index() {
        $surveyCategory = SurveyCategory::orderBy('id', 'DESC')->get();
        $response = [
            'message' => 'All Survey Categories',
            'data' => $surveyCategory,
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function show($id) {
        $surveyCategory = SurveyCategory::findOrFail($id);
        $response = [
            'message' => 'Survey Category by id',
            'data' => $surveyCategory,
        ];
        return response()->json($response, Response::HTTP_OK);
    }
}
