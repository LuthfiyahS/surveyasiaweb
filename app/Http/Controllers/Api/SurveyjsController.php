<?php

namespace App\Http\Controllers\Api;

use App\Models\CompletedSurveyjs;
use App\Models\UsersProfile;
use App\Models\Surveyjs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Http;
/**
 * Controller for handle Public & Member Survey
 *
 * @since batch 3
 */
class SurveyjsController extends Controller
{
    public function index() {
        $survey = Surveyjs::orderBy('id', 'DESC')->get();
        $response = [
            'message' => 'List surveyjs',
            'data' => $survey,
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function show($id) {
        $survey = Surveyjs::findOrFail($id);
        $response = [
            'message' => 'Detail Survey',
            'data' => $survey,
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    public function showbyid($id) {
        $survey = Surveyjs::where('surveyjs_id',$id)->first();
        $response = [
            'message' => 'Detail Survey',
            'data' => $survey,
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    public function showbyuser($id) {
        $survey = Surveyjs::where('user_id',$id)->get();
        if($survey) {
            $response = [
                'message' => 'List User Survey',
                'data' => $survey,
            ];
        }else{
            $response = [
                'message' => 'List User Survey not found',
            ];
        }
        
        return response()->json($response, Response::HTTP_OK);
    }

    public function getShareableLinkById($id) {
        $data = Surveyjs::where('surveyjs_id',$id)->get(['slug', 'shareable_link']);
        if ($data) {
            $response = [
                'message' => 'success',
                'data' => $data,
            ];
        } else {
            $response = [
                'message' => 'not found',
            ];
        }

        return response()->json($response, Response::HTTP_OK);
    }

    public function updateSlug($id, Request $request) {
        $validator =  Validator::make($request->all(), [
            'slug' => 'required|string',
        ]);

        if ( $validator->fails() ) {
            return response()->json($validator->errors(),
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $isSlugAvailable = Surveyjs::where('slug', $request->get('slug'))->first();

        if ($isSlugAvailable != null) {
            $response = [
                'message' => 'This custom link is taken',
            ];

            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            $request['shareable_link'] = env('APP_URL') . $request['slug'];

            Surveyjs::where('surveyjs_id', $id)->update($request->all());

            $response = [
                'message' => 'Slug successfully updated',
            ];

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }

    public function updateTarget($id, Request $request) {
        $validator =  Validator::make($request->all(), [
            'max_attempt' => 'required|numeric',
        ]);

        if ( $validator->fails() ) {
            return response()->json($validator->errors(),
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        try {
            Surveyjs::where('surveyjs_id', $id)->update($request->all());

            $response = [
                'message' => 'Target successfully updated',
            ];

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }


    public function store(Request $request) {
        $validator =  Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'survey_id' => 'required|string',
            'survey_name' => 'required|string',
            'survey_resultsId' => 'required|string',
            'survey_postId' => 'required|string',
            'category_id' => 'required|numeric',
            'status' => 'required|numeric',
            'type' => 'required|string'
        ]);

        if($validator->fails()) { 
            return response()->json($validator->errors(),
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $request['slug'] = Str::slug($request['survey_name'], '-') . '-' . substr($request['survey_id'], 0, 5);
            $request['shareable_link'] = env('APP_URL') . $request['slug'];

            $survey = Surveyjs::create($request->all());
            $response = [
                'message' => 'New survey has been created',
                'data' => $survey
            ];

            return response()->json($response, Response::HTTP_CREATED);
            
        }catch(QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }

    public function updateByCollectRespondent(Request $request, $id) {
        $survey = SurveyJs::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'province' => 'required',
            'city' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
         try {
            $survey->update($request->all());
            $response = [
                'message' => 'Data berhasil di update',
                'data' => $survey
            ];
        
            return response()->json($response, Response::HTTP_OK);

         } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
         }
    }

    public function deletebyid($id) {
        $survey = Surveyjs::where('survey_id',$id)->delete();
        $response = [
            'message' => `Delete Survey ${id} Successful`,
            'data' => null,
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    public function completedSurvey(Request $request) {
        $validator =  Validator::make($request->all(), [
            'surveyjs_id' => 'required',
            'respondent_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(),
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            CompletedSurveyjs::create($request->all());
            
            //update poin user if completed survey
            $survey = Surveyjs::where('surveyjs_id', $request->surveyjs_id)->first();
            $poinsurvey = $survey->reward_point;

            $user = UsersProfile::where('user_id', $request->respondent_id)->first();
            $poinuser = $user->point+$poinsurvey;
            UsersProfile::where('user_id', $request->respondent_id)->update(['point'=> $poinuser]);
            $response = [
                'message' => 'Success',
            ];

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }

    
    public function deleteresultbyid($id) {
        $survey = CompletedSurveyjs::where('surveyjs_id',$id)->delete();
        $response = [
            'message' => `Delete All Result Survey ${id} Successful`,
            'data' => null,
        ];
        return response()->json($response, Response::HTTP_OK);
    }

}

