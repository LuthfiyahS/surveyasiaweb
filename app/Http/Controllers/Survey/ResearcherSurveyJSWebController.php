<?php

namespace App\Http\Controllers\Survey;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Surveyjs;
use Illuminate\Http\Request;
use Http;

class ResearcherSurveyJSWebController extends Controller
{
    public function showSurveyCreator($id) {
        $survey_name = $this->getSurveyName($id);
        $type = $this->getSurveyType($id);
        return view('researcher.surveyjs.creator')->with('survey', ['id' => $id, 'survey_name' => $survey_name,'type'=>$type]);
    }

    public function showCollectRespondent(User $user, Request $request, $id) {
        $survey_name = $this->getSurveyName($id);
        $survey_gender = $this->getSurveyGender($id);
        $max_attempt = $this->getSurveyMaxAttempt($id);
        $max_age = $this->getSurveyMaxAge($id);
        $min_age = $this->getSurveyMinAge($id);
        $province = $this->getSurveyProvince($id);
        $city = $this->getSurveyCity($id);
        $type = $this->getSurveyType($id);
        return view('researcher.surveyjs.collect-respondent')->with('survey', ['id' => $id, 'survey_name' => $survey_name, 'user'=> $user, 'survey_gender'=>$survey_gender, 'max_attempt'=>$max_attempt,'min_age'=>$min_age, 'max_age'=>$max_age,'province'=>$province, 'city'=>$city,'type'=>$type]);
    }

    public function showStatusSurvey($id) {
        $survey_name = $this->getSurveyName($id);
        $type = $this->getSurveyType($id);
        return view('researcher.surveyjs.status-survey')->with('survey', ['id' => $id, 'survey_name' => $survey_name,'type'=>$type]);
    }


    public function showLinkSurvey($id) {
        $survey_name = $this->getSurveyName($id);
        $type = $this->getSurveyType($id);
        return view('researcher.surveyjs.link-survey')->with('survey', ['id' => $id, 'survey_name' => $survey_name,'type'=>$type]);
    }


    public function showReport($id) {
        $survey_name = $this->getSurveyName($id);
        $survey_postId = $this->getSurveyPostId($id);
        $type = $this->getSurveyType($id);
        return view('researcher.surveyjs.report')->with('survey', ['id' => $id, 'survey_name' => $survey_name, 'survey_postId' => $survey_postId,'type'=>$type]);
    }

    public function deleteSurvey($id) {
        Surveyjs::where('surveyjs_id', $id)->delete();
        $accessKey = env('ACCESS_KEY_SURVEYJS');
        Http::delete("https://api.surveyjs.io/private/Surveys/delete/${id}?accessKey=${accessKey}");
        return back();
    }

    /* Function Helper (layer Repositories) */

    public function getSurveyName($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('surveyjs_name')->first();

        return $survey_name_db['surveyjs_name'];
    }

    public function getSurveyPostId($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('surveyjs_postId')->first();

        return $survey_name_db['surveyjs_postId'];
    }

    public function getSurveyGender($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('gender')->first();

        return $survey_name_db['gender'];
    }

    public function getSurveyMaxAttempt($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('max_attempt')->first();

        return $survey_name_db['max_attempt'];
    }
    
    public function getSurveyMaxAge($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('max_age')->first();

        return $survey_name_db['max_age'];
    }

    public function getSurveyMinAge($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('min_age')->first();

        return $survey_name_db['min_age'];
    }

    public function getSurveyProvince($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('province')->first();

        return $survey_name_db['province'];
    }

    public function getSurveyCity($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('city')->first();

        return $survey_name_db['city'];
    }

    public function getSurveyType($id)
    {
        $survey_name_db = Surveyjs::where('surveyjs_id', $id)->get('type')->first();

        return $survey_name_db['type'];
    }
}
