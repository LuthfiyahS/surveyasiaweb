<?php

namespace App\Http\Controllers\Respondent;

use App\Http\Controllers\Controller;
use App\Models\CompletedSurveyjs;
use App\Models\Surveyjs;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Controller for handle SurveyJS Web
 *
 * @since Batch 3
 */
class SurveyJSWebController extends Controller
{
    /**
     * Load file blade survey public
     *
     * Check if guest -> 200
     * Check if user not respondent -> 403
     * Check if user not guest -> get user id
     * Get Name, PostId, Type from table Surveyjs using eloquent
     * Check if its survey member -> redirect action to member
     * Pass name, id, post id to view blade
     * name for title
     * id and post id for props vue js
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse
     *
     * @since Batch 3
     */
    public function viewSurveyPublic($id)
    {
        $this->isNotRespondent();

        $cekRole = Auth::user();
        if ($cekRole != null) {
            $respondentId = Auth::user()->id;
        }

        $survey = Surveyjs::select('surveyjs_name', 'surveyjs_postId', 'type')->where('surveyjs_id',$id)->get()->first();

        if ($survey['type'] == 'paid') {
            return redirect()->action([SurveyJSWebController::class, 'viewSurveyMember'], ['id' => $id]);
        }

        $name = $survey['surveyjs_name'];
        $postId = $survey['surveyjs_postId'];

        return view('respondent.surveyjs.survey')->with('survey', ['id' => $id, 'name' => $name, 'post_id' => $postId, 'respondent_id' => $respondentId]);
    }

    /**
     * Load file blade survey member
     *
     * Check if respondent complete survey -> 403
     * Get Name, PostId from table Surveyjs using eloquent
     * Check if its survey public -> redirect action to public
     * Check if guest -> 401
     * Check if user not respondent -> 403
     * Pass name, id, post id to view blade
     * name for title
     * id and post id for props vue js
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse
     *
     * @since Batch 3
     */
    public function viewSurveyMember($id)
    {
        $cekRole = Auth::user();
        if ($cekRole != null) {
            $respondentId = Auth::user()->id;
            //$respondentIdDB = CompletedSurveyjs::select('respondent_id')->where('surveyjs_id', $id)->get()->first();
            $respondentIdDB = CompletedSurveyjs::select('respondent_id')->where('surveyjs_id', $id)->where('respondent_id', $respondentId)->get()->first();

            if ($respondentIdDB !== null) {
                abort(403, "Ups! Anda telah mengisi survey ini.");
            }
        }

        $survey = Surveyjs::select('surveyjs_name', 'surveyjs_postId', 'type')->where('surveyjs_id',$id)->get()->first();

        if ($survey['type'] == 'free') {
            return redirect()->action([SurveyJSWebController::class, 'viewSurveyPublic'], ['id' => $id]);
        }

        $this->isGuest();
        $this->isNotRespondent();

        $name = $survey['surveyjs_name'];
        $postId = $survey['surveyjs_postId'];

        return view('respondent.surveyjs.survey')->with('survey', ['id' => $id, 'name' => $name, 'post_id' => $postId, 'respondent_id' => $respondentId]);
    }

    /**
     * Get Survey Id, Type by Slug from param
     *
     * check if survey null -> 404
     * check if its member survey -> redirect action to member
     * check if its public survey -> redirect action to public
     *
     * @param $slug
     * @return RedirectResponse
     *
     * @since Batch 3
     */
    public function getSurveyIdBySlug($slug): RedirectResponse
    {
        $survey = Surveyjs::select('surveyjs_id', 'type')->where('slug', $slug)->get('surveyjs_id')->first();

        if ($survey == null) {
            return abort(404);
        }

        $id = $survey['surveyjs_id'];
        $tipeSurvey = $survey['type'];

        if ($tipeSurvey == "paid") {
            return redirect()->action([SurveyJSWebController::class, 'viewSurveyMember'], ['id' => $id]);
        }

        return redirect()->action([SurveyJSWebController::class, 'viewSurveyPublic'], ['id' => $id]);
    }

    /**
     * Role Check
     *
     * Check if guest -> 401
     *
     * @return void
     */
    public function isGuest() {
        $guest = Auth::guest();

        if ($guest) {
            abort(401, "Ups! Anda harus login sebagai Respondent untuk mengisi Survey Member.");
        }
    }

    /**
     * Check User Login and Not Respondent
     *
     * Check if user not null (login)
     * Check if user is not respondent -> 403
     *
     * @return void
     */
    public function isNotRespondent() {
        $cekRole = Auth::user();

        if ($cekRole != null) {
            $role = Auth::user()->role_id;
            if ($role != 3) {
                abort(403, "Ups! Anda harus login sebagai Respondent untuk mengisi Survey.");
            }
        }
    }

    public function viewPageDetailSurveyJS($slug) {
        $surveyjs = Surveyjs::where('slug', '=', $slug)->get()->first();
        $data = [
          'surveyjs' => $surveyjs
        ];

        return view('respondent.surveyjs.detail', $data);
    }
}
